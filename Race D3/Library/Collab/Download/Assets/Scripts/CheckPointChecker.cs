﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointChecker : MonoBehaviour
{

    public CarController car;


    private void Start()
    {
        car = GameObject.FindGameObjectWithTag("Player").GetComponent<CarController>();   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Checkpoint")
        {
            car.CheckPointHit(other.GetComponent<CheckPoints>().cpNumber);
        }
    }
}
