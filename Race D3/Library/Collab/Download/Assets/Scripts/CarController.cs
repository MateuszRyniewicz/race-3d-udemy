﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{

    public float maxSpeed;
    public Rigidbody rb;

    public float forwardAceteleration = 8f, reverseAceeteleration = 4f;
    private float speedInput;

    public float turnStrenght = 180f;
    private float turnInput;

    private bool grounded;

    public Transform groundRayPoint, groundRayPoint2;
    public LayerMask whatIsGrounded;
    public float groundRayLenght = 0.75f;

    private float dragOnGround;
    public float gravityMod = 10f;

    public Transform leftFronWheel, rightFrontWheel;
    public float maxWheelTurn = 25f;

    public ParticleSystem [] dustTrail;
    public float maxEmission = 25f, emissionFadeSpeed = 20f;
    private float emissionRate =0;

    public AudioSource engineSound, skidSound;
    public float skidfadeSpeed=2f;

    public int nextCheckpoint;
    public int currentLap;

    public float labTime, bestlabTime;

    public bool isAI;

    public int currentTarget;
    private Vector3 targetPoint;
    public float aiAccelerateSpeed = 1f, aiTurnSpeed = 0.8f, aiReachPointRange=5f, aiPointVariante=3f, aiMaxTurn = 30f;
    private float aiSpeedInput, aiSpeedMod;
    // Start is called before the first frame update
    void Start()
    {
        
        rb.transform.parent = null;
        dragOnGround = rb.drag;

        if (isAI)
        {
            targetPoint = RaceManager.instance.allCheckPoint[currentTarget].transform.position;
            RandomiseAITarget();

            aiSpeedMod = Random.Range(1.2f, 2.2f);
        }

        UiManager.instance.labCurrentText.text = currentLap + "/" + RaceManager.instance.totalLabs;

    }

    // Update is called once per frame
    void Update()
    {
        labTime += Time.deltaTime;
        if (!isAI)
        {


            var ts = System.TimeSpan.FromSeconds(labTime);
            UiManager.instance.currentLabTimeText.text = string.Format("{00:00}m{01:00}.{2:000}s", ts.Minutes, ts.Seconds, ts.Milliseconds);

            speedInput = 0;

            if (Input.GetAxis("Vertical") > 0)
            {
                speedInput = Input.GetAxis("Vertical") * forwardAceteleration;
            }
            else if (Input.GetAxis("Vertical") < 0)
            {
                speedInput = Input.GetAxis("Vertical") * reverseAceeteleration;
            }


            turnInput = Input.GetAxis("Horizontal");

            /* 
              if (grounded && Input.GetAxis("Vertical") != 0)
             {
                 transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, turnInput * turnStrenght * Time.deltaTime * Mathf.Sign(speedInput) * (rb.velocity.magnitude / maxSpeed), 0));
             }
             */
        }
        else
        {
            targetPoint.y = transform.position.y;

            if (Vector3.Distance(transform.position, targetPoint) < aiReachPointRange)
            {
                SetNextAITarget();

            }

            Vector3 targetDirection = targetPoint - transform.position;
            float angle = Vector3.Angle(targetDirection, transform.forward);

            Vector3 localPos = transform.InverseTransformPoint(targetPoint);
            if(localPos.x < 0f)
            {
                angle = -angle;
            }

            turnInput = Mathf.Clamp(angle / aiMaxTurn , -1f, 1f);

            if(Mathf.Abs(angle) < aiMaxTurn)
            {
                aiSpeedInput = Mathf.MoveTowards(aiSpeedInput, 1f, aiAccelerateSpeed);
            }
            else
            {
                aiSpeedInput = Mathf.MoveTowards(aiSpeedInput, aiTurnSpeed, aiAccelerateSpeed);
            }

            
            speedInput = aiSpeedInput * forwardAceteleration * aiSpeedMod;
        }





        //truning the wheels

        leftFronWheel.localRotation = Quaternion.Euler(leftFronWheel.transform.localRotation.eulerAngles.x, (turnInput * maxWheelTurn) -180f, leftFronWheel.transform.localRotation.eulerAngles.z);
        rightFrontWheel.localRotation = Quaternion.Euler(rightFrontWheel.transform.localRotation.eulerAngles.x, (turnInput * maxWheelTurn), rightFrontWheel.transform.localRotation.eulerAngles.z);

        //control particle emission

        emissionRate = Mathf.MoveTowards(emissionRate, 0f, emissionFadeSpeed * Time.deltaTime);

        if (grounded && (Mathf.Abs(turnInput) >.5f || (rb.velocity.magnitude < maxSpeed * 0.5f && rb.velocity.magnitude !=0)))
        {
            emissionRate = maxEmission;
        }

        if (rb.velocity.magnitude <= 0.5f)
        {
            emissionRate = 0;
        }

        for(int i = 0; i < dustTrail.Length; i++)
        {
            var emissionModule = dustTrail[i].emission;

            emissionModule.rateOverTime = emissionRate;
        }

        if (engineSound != null)
        {
            engineSound.pitch = 1f + ((rb.velocity.magnitude / maxSpeed) * 2);
        }

        if (skidSound != null)
        {
            if (Mathf.Abs(turnInput) > 0.5f)
            {
                skidSound.volume = 1;
            }
            else
            {
                skidSound.volume = Mathf.MoveTowards(skidSound.volume, 0f, skidfadeSpeed * Time.deltaTime);
            }
            
        }

     //   transform.position = rb.position;
    }
    void FixedUpdate()
    {
        grounded = false;

        RaycastHit hit;
        Vector3 normalTarget = Vector3.zero;

        if(Physics.Raycast(groundRayPoint.position,-transform.up, out hit, groundRayLenght, whatIsGrounded))
        {
            grounded = true;

            normalTarget = hit.normal;

        }

        if(Physics.Raycast(groundRayPoint2.position,-transform.up, out hit, groundRayLenght, whatIsGrounded))
        {
            grounded = true;

            normalTarget = (normalTarget + hit.normal) / 2f;
        }

        //when on grounded rotate to much the normal

        if (grounded)
        {
            transform.rotation = Quaternion.FromToRotation(transform.up, normalTarget) * transform.rotation;
        }
        // acceleration the car
        if (grounded)
        {
            rb.drag = dragOnGround;

            rb.AddForce(transform.forward * speedInput * 1000f);
        }
        else
        {
            rb.drag = 0.1f;

            rb.AddForce(-Vector3.up * gravityMod * 100f);
        }

        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }

       // Debug.Log("speed: " + rb.velocity.magnitude);

        transform.position = rb.position;

        if (grounded && speedInput != 0)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, turnInput * turnStrenght * Time.deltaTime * Mathf.Sign(speedInput) * (rb.velocity.magnitude / maxSpeed), 0));
        }
    }

    public void CheckPointHit(int cpNumber)
    {
        if (cpNumber == nextCheckpoint)
        {

            nextCheckpoint++;


            if (nextCheckpoint == RaceManager.instance.allCheckPoint.Length)
            {
                nextCheckpoint = 0;
                RaceCompleted();
            }
        }

        else if (isAI)
        {
            if (cpNumber == currentTarget)
            {
                SetNextAITarget();
            }
        }
        
    }

    public void SetNextAITarget()
    {
        currentTarget++;

        if (currentTarget >= RaceManager.instance.allCheckPoint.Length)
        {
            currentTarget = 0;
        }

        targetPoint = RaceManager.instance.allCheckPoint[currentTarget].transform.position;
        RandomiseAITarget();
    }


    public void RaceCompleted()
    {
        currentLap++;

        if(labTime < bestlabTime || bestlabTime == 0)
        {
            bestlabTime = labTime;
        }

        labTime = 0;


        if (!isAI)
        {

            var ts = System.TimeSpan.FromSeconds(bestlabTime);

            UiManager.instance.bestLabTimeText.text = string.Format("{00:00}m{01:00}.{2:000}s", ts.Minutes, ts.Seconds, ts.Milliseconds);
            UiManager.instance.labCurrentText.text = currentLap + "/" + RaceManager.instance.totalLabs;
        }
    }

    public void RandomiseAITarget()
    {
        targetPoint += new Vector3(Random.Range(-aiPointVariante, aiPointVariante), 0f, Random.Range(-aiPointVariante,aiPointVariante));
    }

    
}
