﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceManager : MonoBehaviour
{
    public static RaceManager instance;


    public void Awake()
    {
        instance = this;
    }

    public CheckPoints[] allCheckPoint;

    public int totalLabs;


    public CarController playerCar;
    public List<CarController> allAICars = new List<CarController>();
    public int playerPosition;
    public float timeBetwwenPosCheck = 0.2f;
    private float posCheckPosCunter;

    public float aiDefaultSpeed = 30f, playerDefaultSpeed = 30f, rubberBrandSpeedMod = 3.5f, rubBandAcell = 0.5f;

    // Start is called before the first frame update
    void Start()
    {



            for (int i = 0; i < allCheckPoint.Length; i++)
            {
                allCheckPoint[i].cpNumber = i;
            } 
     
   


    }

    // Update is called once per frame
    void Update()
    {
        posCheckPosCunter -= Time.deltaTime;

        if (posCheckPosCunter <= 0)
        {


            playerPosition = 1;

            foreach (CarController aicar in allAICars)
            {
                if (aicar.currentLap > playerCar.currentLap)
                {
                    playerPosition++;
                }
                else if (aicar.currentLap == playerCar.currentLap)
                {
                    if (aicar.nextCheckpoint > playerCar.nextCheckpoint)
                    {
                        playerPosition++;
                    }
                    else if (aicar.nextCheckpoint == playerCar.nextCheckpoint)
                    {
                        if (Vector3.Distance(aicar.transform.position, allCheckPoint[aicar.nextCheckpoint].transform.position) < Vector3.Distance(playerCar.transform.position, allCheckPoint[aicar.nextCheckpoint].transform.position))
                        {
                            playerPosition++;
                        }
                    }
                }

            }

            posCheckPosCunter = timeBetwwenPosCheck;

            UiManager.instance.positionText.text = playerPosition + "/" + (allAICars.Count + 1);
        }
    }
}
