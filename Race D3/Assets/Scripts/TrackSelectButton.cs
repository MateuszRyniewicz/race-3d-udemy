﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrackSelectButton : MonoBehaviour
{

    public string trackSeceneName;

    public Image trackImage;

    public int raceLaps = 4;


    public GameObject unlockText;

    private bool isLocked;

    public string trackToUnlockOnWin;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey(trackSeceneName + "_unlocked"))
        {
            isLocked = true;

            trackImage.color = Color.gray;
            unlockText.SetActive(true);
        }
    }
        public void SelectTrack()
        {
        if (!isLocked)
        {


            RaceInfoManager.instance.trackToLoad = trackSeceneName;
            RaceInfoManager.instance.noOfLaps = raceLaps;
            RaceInfoManager.instance.trackSprite = trackImage.sprite;


            MainMenu.instance.trackSelectImage.sprite = trackImage.sprite;


            MainMenu.instance.CloseTrackSelect();

            RaceInfoManager.instance.trackToUnlock = trackToUnlockOnWin;
         }

        }   
    
}
