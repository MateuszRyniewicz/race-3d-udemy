﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    private void Awake()
    {
        instance = this;
    }

    public TMP_Text labCurrentText, bestLabTimeText, currentLabTimeText, positionText, countdownText, goText, raceResultText;

    public GameObject resultScreen, pouseScreen, trackUnlockedMessage;

    public bool isPoused;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PouseUnPouse();
        }
    }
    public void PouseUnPouse()
    {
        isPoused = !isPoused;

        pouseScreen.SetActive(isPoused);

        if (isPoused)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    public void ExitRace()
    {
        Time.timeScale = 1f;

        RaceManager.instance.ExitRace();
    }
    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}
