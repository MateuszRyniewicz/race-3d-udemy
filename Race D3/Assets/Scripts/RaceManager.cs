﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RaceManager : MonoBehaviour
{
    public static RaceManager instance;


    public void Awake()
    {
        instance = this;
    }

    public CheckPoints[] allCheckPoint;

    public int totalLabs;


    public CarController playerCar;
    public List<CarController> allAICars = new List<CarController>();
    public int playerPosition;
    public float timeBetwwenPosCheck = 0.2f;
    private float posCheckPosCunter;

    public float aiDefaultSpeed = 30f, playerDefaultSpeed = 30f, rubberBrandSpeedMod = 3.5f, rubBandAcell = 0.5f;


    public bool isStarting;
    public float timeBetweenStartCount = 1f;
    private float startCounter;
    public int countdownCurrent = 3;

    public int playStartPosition, aiNumberToSpawn;
    public Transform[] startPoints;
    public List<CarController> carsToSpown = new List<CarController>();

    public bool raceCompleted;

    public string raceCompleteScene;

    // Start is called before the first frame update
    void Start()
    {
        totalLabs = RaceInfoManager.instance.noOfLaps;
        aiNumberToSpawn = RaceInfoManager.instance.noOfAI;

            for (int i = 0; i < allCheckPoint.Length; i++)
            {
                allCheckPoint[i].cpNumber = i;
            } 
     
   
        isStarting = true;
        startCounter = timeBetweenStartCount;

        UiManager.instance.countdownText.text = countdownCurrent + "!" ;

        playStartPosition =  Random.Range(0, aiNumberToSpawn + 1);


        playerCar = Instantiate(RaceInfoManager.instance.recerToUse, startPoints[playStartPosition].position, startPoints[playStartPosition].rotation);
        playerCar.isAI = false;
        playerCar.GetComponent<AudioListener>().enabled = true;

        CameraSwitcher.instance.SetTarget(playerCar);

        //playerCar.transform.position = startPoints[playStartPosition].position;
        //playerCar.rb.transform.position = startPoints[playStartPosition].position;

        for (int i = 0; i < aiNumberToSpawn + 1 ; i++)
        {
            if (i != playStartPosition)
            {
                int selectedCar = Random.Range(0, carsToSpown.Count);

                 allAICars.Add(Instantiate(carsToSpown[selectedCar], startPoints[i].position, startPoints[i].rotation));

                if (carsToSpown.Count > aiNumberToSpawn - i)
                {

                    carsToSpown.RemoveAt(selectedCar);
                }
            }
        }

        UiManager.instance.positionText.text = (playStartPosition + 1) + "/" + (allAICars.Count + 1);

    }

    // Update is called once per frame
    void Update()
    {

        if (isStarting)
        {
            startCounter -= Time.deltaTime;
            if (startCounter <= 0)
            {
                countdownCurrent--;
                startCounter = timeBetweenStartCount;

                UiManager.instance.countdownText.text = countdownCurrent + "!";

                if (countdownCurrent == 0)
                {
                    isStarting = false;

                    UiManager.instance.countdownText.gameObject.SetActive(false);
                    UiManager.instance.goText.gameObject.SetActive(true);
                }
            }
        }
        else
        {

            posCheckPosCunter -= Time.deltaTime;

            if (posCheckPosCunter <= 0)
            {


                playerPosition = 1;

                foreach (CarController aicar in allAICars)
                {
                    if (aicar.currentLap > playerCar.currentLap)
                    {
                        playerPosition++;
                    }
                    else if (aicar.currentLap == playerCar.currentLap)
                    {
                        if (aicar.nextCheckpoint > playerCar.nextCheckpoint)
                        {
                            playerPosition++;
                        }
                        else if (aicar.nextCheckpoint == playerCar.nextCheckpoint)
                        {
                            if (Vector3.Distance(aicar.transform.position, allCheckPoint[aicar.nextCheckpoint].transform.position) < Vector3.Distance(playerCar.transform.position, allCheckPoint[aicar.nextCheckpoint].transform.position))
                            {
                                playerPosition++;
                            }
                        }
                    }

                }


                posCheckPosCunter = timeBetwwenPosCheck;

                UiManager.instance.positionText.text = playerPosition + "/" + (allAICars.Count + 1);
            }


            //manager rubber bending

            if (playerPosition == 1)
            {

                foreach (CarController aiCar in allAICars)
                {
                    aiCar.maxSpeed = Mathf.MoveTowards(aiCar.maxSpeed, aiDefaultSpeed + rubberBrandSpeedMod, rubBandAcell * Time.deltaTime);
                }


                playerCar.maxSpeed = Mathf.MoveTowards(playerCar.maxSpeed, playerDefaultSpeed - rubberBrandSpeedMod, rubBandAcell * Time.deltaTime);
            }
            else
            {
                foreach (CarController aiCar in allAICars)
                {
                    aiCar.maxSpeed = Mathf.MoveTowards(aiCar.maxSpeed, aiDefaultSpeed - (rubberBrandSpeedMod * ((float)playerPosition / ((float)allAICars.Count + 1))), rubBandAcell * Time.deltaTime);
                }


                playerCar.maxSpeed = Mathf.MoveTowards(playerCar.maxSpeed, playerDefaultSpeed + (rubberBrandSpeedMod * ((float)playerPosition / ((float)allAICars.Count + 1))), rubBandAcell * Time.deltaTime);
            }
        }
    }

    public void FinishedRace()
    {
        raceCompleted = true;

        switch (playerPosition)
        {
            case 1:
                UiManager.instance.raceResultText.text = " You Finished 1st ";
                if(RaceInfoManager.instance.trackToUnlock != "")
                {
                    if (!PlayerPrefs.HasKey(RaceInfoManager.instance.trackToUnlock + "_unlocked"))
                    {

                        PlayerPrefs.SetInt(RaceInfoManager.instance.trackToUnlock + "_unlocked", 1);
                        UiManager.instance.trackUnlockedMessage.SetActive(true);
                    }
                }


                break;
            case 2:
                UiManager.instance.raceResultText.text = " You Finished 2nd ";
                break;
            case 3:
                UiManager.instance.raceResultText.text = " You Finished 3rd ";
                break;
            default:
                UiManager.instance.raceResultText.text = " You Finished " + playerPosition + "th"; 
                break;
        }

        UiManager.instance.resultScreen.SetActive(true);
    }

    public void ExitRace()
    {
        SceneManager.LoadScene(raceCompleteScene);

    }
}
