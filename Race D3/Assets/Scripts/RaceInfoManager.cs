﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceInfoManager : MonoBehaviour
{

    public static RaceInfoManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }

    public string trackToLoad;
    public CarController recerToUse;
    public int noOfAI;
    public int noOfLaps;

    public bool eneterdRace;
    public Sprite trackSprite, raceSprite;

    public string trackToUnlock;

   
}
