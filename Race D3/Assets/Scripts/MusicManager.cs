﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource musicPlayer;
    public AudioClip[] potencialMusic; 


    // Start is called before the first frame update
    void Start()
    {
        musicPlayer.clip = potencialMusic[Random.Range(0, potencialMusic.Length)];
        musicPlayer.Play();
    }
   
}
