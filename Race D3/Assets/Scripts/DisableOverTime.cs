﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOverTime : MonoBehaviour
{

    public float timeToDissable;
   
    // Update is called once per frame
    void Update()
    {
        timeToDissable -= Time.deltaTime;
        if (timeToDissable <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
