﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnCollision : MonoBehaviour
{
    public AudioSource soundToPlay;

    LayerMask groundLayerNO = 8;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.layer != 8)
        {


            soundToPlay.Stop();
            soundToPlay.pitch = Random.Range(0.6f, 1.3f);
            soundToPlay.Play();
        }
    }
}
